package Exercice2;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.Assert.*;

public class ReservationTest
{
	
	@Test
	public void testReservationValide()
	{
		//Arrange
		List<Medecin> medecins = new ArrayList<Medecin>();
		List<Patient> patients = new ArrayList<Patient>();
		Medecin m1 = new Medecin();
		Medecin m2 = new Medecin();
		LocalDate d1 = LocalDate.now().minusDays(1);
		LocalDate d2 = LocalDate.now();
		Patient p1 = new Patient();
		Patient p2 = new Patient();
		Reservation r1 = new Reservation(d1, m1, p1);
		m1.rendezVous.add(r1);
		p1.rendezVous.add(r1);
		Reservation r2 = new Reservation(d2, m1, p1);
		m1.rendezVous.add(r2);
		p1.rendezVous.add(r2);
		medecins.add(m1);
		medecins.add(m2);
		patients.add(p1);
		patients.add(p2);
		SystemeDeReservation systemeDeReservation = new SystemeDeReservation(medecins, patients);
		
		//Act
		boolean reservationOk = systemeDeReservation.reserve(p2, m2, d2);
		
		//Assert
		assertTrue(reservationOk);
	}
	
	@Test
	public void testReservationPatientInvalide()
	{
		List<Medecin> medecins = new ArrayList<Medecin>();
		List<Patient> patients = new ArrayList<Patient>();
		Medecin m1 = new Medecin();
		Medecin m2 = new Medecin();
		LocalDate d1 = LocalDate.now().minusDays(1);
		LocalDate d2 = LocalDate.now();
		Patient p1 = new Patient();
		Patient p2 = new Patient();
		Patient p3 = new Patient(); //ce patient ne sera pas ajouté à la liste
		patients.add(p1);
		patients.add(p2);
		medecins.add(m1);
		medecins.add(m2);
		Reservation r1 = new Reservation(d1, m1, p1);
		m1.rendezVous.add(r1);
		p1.rendezVous.add(r1);
		Reservation r2 = new Reservation(d2, m1, p1);
		m1.rendezVous.add(r2);
		p1.rendezVous.add(r2);
		SystemeDeReservation systemeDeReservation = new SystemeDeReservation(medecins, patients);
		
		boolean reservationOk = systemeDeReservation.reserve(p3, m2, d2);
		
		assertFalse(reservationOk);
		
	}
	
	@Test
	public void testReservationMedecinInvalide()
	{
		List<Medecin> medecins = new ArrayList<Medecin>();
		List<Patient> patients = new ArrayList<Patient>();
		Medecin m1 = new Medecin();
		Medecin m2 = new Medecin();
		Medecin m3 = new Medecin(); //ce médecin ne sera pas ajouté à la liste
		LocalDate d1 = LocalDate.now().minusDays(1);
		LocalDate d2 = LocalDate.now();
		Patient p1 = new Patient();
		Patient p2 = new Patient();
		patients.add(p1);
		patients.add(p2);
		medecins.add(m1);
		medecins.add(m2);
		Reservation r1 = new Reservation(d1, m1, p1);
		m1.rendezVous.add(r1);
		p1.rendezVous.add(r1);
		Reservation r2 = new Reservation(d2, m1, p1);
		m1.rendezVous.add(r2);
		p1.rendezVous.add(r2);
		SystemeDeReservation systemeDeReservation = new SystemeDeReservation(medecins, patients);
		
		boolean reservationOk = systemeDeReservation.reserve(p2, m3, d2);
		
		assertFalse(reservationOk);
		
	}
	
	@Test
	public void testReservationDateIndisponible()
	{
		List<Medecin> medecins = new ArrayList<Medecin>();
		List<Patient> patients = new ArrayList<Patient>();
		Medecin m1 = new Medecin();
		Medecin m2 = new Medecin();
		LocalDate d1 = LocalDate.now().plusDays(1);
		LocalDate d2 = LocalDate.now();
		Patient p1 = new Patient();
		Patient p2 = new Patient();
		patients.add(p1);
		patients.add(p2);
		medecins.add(m1);
		medecins.add(m2);
		Reservation r1 = new Reservation(d1, m1, p1);
		m1.rendezVous.add(r1);
		p1.rendezVous.add(r1);
		SystemeDeReservation systemeDeReservation = new SystemeDeReservation(medecins, patients);
		
		boolean reservationOk = systemeDeReservation.reserve(p2, m1, d1); //réservation avec le même médecin, à la même date que la première, mais pour un autre patient
		
		assertFalse(reservationOk);
		
	}
	
	@Test
	public void testReservationPatientOccupe()
	{
		List<Medecin> medecins = new ArrayList<Medecin>();
		List<Patient> patients = new ArrayList<Patient>();
		Medecin m1 = new Medecin();
		Medecin m2 = new Medecin();
		LocalDate d1 = LocalDate.now().plusDays(1);
		LocalDate d2 = LocalDate.now();
		Patient p1 = new Patient();
		Patient p2 = new Patient();
		patients.add(p1);
		patients.add(p2);
		medecins.add(m1);
		medecins.add(m2);
		Reservation r1 = new Reservation(d1, m1, p1);
		Reservation r2 = new Reservation(d2, m1, p2);
		m1.rendezVous.add(r1);
		p1.rendezVous.add(r1);
		m1.rendezVous.add(r2);
		p2.rendezVous.add(r2);
		SystemeDeReservation systemeDeReservation = new SystemeDeReservation(medecins, patients);
		
		boolean reservationOk = systemeDeReservation.reserve(p2, m2, d2); 
		
		assertFalse(reservationOk);
		
	}
	
	@ParameterizedTest
    @CsvSource({"p2,m1,2022-04-01T10:00:00", "p1,m2,2022-04-02T11:00:00", "p2,m2,2022-04-02T13:00:00"})
    void testReservationParametre(Patient input1, Medecin input2, LocalDate input3)
    {
		List<Medecin> medecins = new ArrayList<Medecin>();
		List<Patient> patients = new ArrayList<Patient>();
		Medecin m1 = new Medecin();
		Medecin m2 = new Medecin();
		Patient p1 = new Patient();
		Patient p2 = new Patient();
		patients.add(p1);
		patients.add(p2);
		medecins.add(m1);
		medecins.add(m2);
		SystemeDeReservation systemeDeReservation = new SystemeDeReservation(medecins, patients);
        
		
		assertTrue(systemeDeReservation.reserve(input1, input2, input3));
    }
	
	
	
}
