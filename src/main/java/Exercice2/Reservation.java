package Exercice2;

import java.time.LocalDate;

public class Reservation
{
	
	int id;
	Medecin medecin;
	Patient patient;
	LocalDate dateHeure;
	
	Reservation(LocalDate _dateHeure, Medecin _medecin, Patient _patient)
	{
		this.dateHeure = _dateHeure;
		this.patient = _patient;
		this.medecin = _medecin;
	}
	
}