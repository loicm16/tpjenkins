package Exercice2;

import java.util.List;
import java.util.stream.Collectors;

import org.junit.platform.commons.util.CollectionUtils;

import java.util.ArrayList;
import java.time.LocalDate;


public class SystemeDeReservation
{
	private List<Medecin> medecins = new ArrayList<Medecin>();
	private List<Patient> patients = new ArrayList<Patient>();
	
	SystemeDeReservation(List<Medecin> _medecins, List<Patient> _patients)
	{
		this.medecins = _medecins;
		this.patients = _patients;
	}
	
	public boolean reserve(Patient patient, Medecin medecin, LocalDate dateHeure)
	{
		//le médecin doit exister dans la base
		if(medecins.contains(medecin))
		{
			//le patient aussi
			if(patients.contains(patient))
			{
				//le médecin ne doit pas avoir déjà un rendez-vous à ce moment
				boolean medecinOccupe = false;
				//et ne doit pas avoir + de 4 réservations le même jour
				boolean deja4reservations = false;
				//pour savoir cela, on parcourt tous ses rendez-vous
				int nbReservations = 0;
				for (Reservation reservation : medecin.rendezVous)
				{
			        if (reservation.dateHeure.equals(dateHeure))
			        {
			           medecinOccupe = true;
			        }
			        if(dateHeure == reservation.dateHeure)
			        {
			        	nbReservations++;
			        }
			    }
				if(nbReservations >= 4)
					deja4reservations = true;
				
				if(!medecinOccupe && !deja4reservations)
				{
					//le patient ne doit pas avoir déjà un rendez-vous à ce moment là
					boolean patientOccupe = false;
					//pour savoir cela, on parcourt tous ses rendez-vous
					for (Reservation reservation : patient.rendezVous)
					{
				        if (reservation.dateHeure.equals(dateHeure))
				        {
				           patientOccupe = true;
				        }
				    }
				
					if(!patientOccupe)
					{
						//Toutes les conditions sont réunies, ont peut créer la réservation
						Reservation r = new Reservation(dateHeure, medecin, patient);
						medecin.rendezVous.add(r);
						patient.rendezVous.add(r);
						return true;
					}
					
				}
			}
		}
		//Au moins une condition n'est pas remplie
		return false;	
	}
	
}
