package Exercice1;

public class PaymentProcessor
{

	private CreditCardManager creditCardManager;
	private PaymentGateway paymentGateway;
	
	String processPayment(double montant, String numCart, String cvv)
	{
		creditCardManager = new CreditCardManager();
		paymentGateway = new PaymentGateway();
		
		//vérifie la validité de la carte grâce au CreditCardManager
		//effectue la transaction grâce à la PaymentGateway si la carte et valide
		//retourne une chaîne vide si la carte n'est pas valide, l'identifiant de la transaction sinon
		if(creditCardManager.verifyCard(numCart, cvv))
			return paymentGateway.makePayment(montant);
		else
			return "";
	}
	
}
