package Exercice1;

import org.junit.Test;
import static org.junit.Assert.*;


public class PaymentIntegrationTest
{
	@Test
	public void paymentSucceed()
	{
		//Arrange
		PaymentProcessor paymentProcessor = new PaymentProcessor();
		String idTransaction;
	
		//Act
		idTransaction = paymentProcessor.processPayment(10, "0123456789101112", "123");
		
		//Assert
		assertEquals("AAAAA", idTransaction);
	}
	
	@Test
	public void invalidCard()
	{
		
		//Arrange
		PaymentProcessor paymentProcessor = new PaymentProcessor();
		String idTransaction = "0";
		
		/********************************
		 ****Numéro de carte invalide****
		 ********************************/
		
		//Act
		idTransaction = paymentProcessor.processPayment(10, "0123", "123");
		//Assert
		assertEquals("", idTransaction);
		
		/********************************
		 **********CVV invalide**********
		 ********************************/
		
		//Act
		idTransaction = paymentProcessor.processPayment(10, "01234567891011121", "1");
		//Assert
		assertEquals("", idTransaction);
		
		/********************************
		 *******Les deux invalides*******
		 ********************************/
		
		//Act
		idTransaction = paymentProcessor.processPayment(10, "45678", "12345678");
		//Assert
		assertEquals("", idTransaction);
		
	}
	
}
